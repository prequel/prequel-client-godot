extends Node2D

const field_width = 30
const field_height = 20
const cell_width = 30
const cell_height = 30
const margin = 2


var ar = PoolColorArray()

func _ready():
    var arsize = field_width*field_height
    for a in range(arsize):
        #ar.append(Color(rand_range(0,1),rand_range(0,1),rand_range(0,1)))
        ar.append(Color(0,0,0))
    pass

func _draw():
    draw_rect(Rect2(Vector2(0,0), Vector2((cell_width+margin)*field_width,(cell_height+margin)*field_height)),Color(0.5,0.5,0.5))
    for y in range(field_height):
        for x in range(field_width):
            var c_color = ar[y*field_width+x]
            var c_pos = Vector2(x * (cell_width+margin),y * (cell_height+margin))
            draw_rect(Rect2(c_pos, Vector2(cell_width,cell_height)),c_color)
    pass

func redden(var x,var y):
    ar[y*field_width+x].r=ar[y*field_width+x].r+0.1
    self.update()
    pass

func _input(event):
    var mpos
    var dx = -1
    var dy = -1
    if event is InputEventMouseButton:
        if (!event.is_pressed()):
            mpos = get_global_mouse_position()
            dx = int(mpos.x/(cell_width+margin))
            dy = int(mpos.y/(cell_height+margin))
            print("detected: dx[",dx,"]dy[",dy,"]")
            if (dx in range(0,field_width) && dy in range(0,field_height)): redden(dx,dy)
    pass        
    #elif event is InputEventMouseMotion and dragging:
    #    
#func _process(delta):
#    # Called every frame. Delta is time since last frame.
#    # Update game logic here.
#    pass

