extends Node


const prequel_server_url = 'localhost'
const prequel_server_port = 8000

var online = false
var err = 0
var http = HTTPClient.new()

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    pass

#func _process(delta):
#    # Called every frame. Delta is time since last frame.
#    # Update game logic here.
#    pass



func _on_HTTPRequest_request_completed( result, response_code, headers, body ):
    var json = JSON.parse(body.get_string_from_utf8())
    print("Received http data: ",json.result)
    pass # replace with function body
    
    
var hbc = 0 #heartbeat counter

func _on_Heartbeat_timeout():
    print("Heartbeat ",hbc)
    hbc += 1
    err = http.connect_to_host(prequel_server_url,prequel_server_port)
    if (err != OK):
        online = false
        print("Offline mode")
        pass
    
    var n = 0
    var attempts = 5
    while (http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING) and n<attempts:
        http.poll()
        print("Connecting..Attempts remaining: ",attempts-n)
        OS.delay_msec(500)
        n += 1

    #print("status:",http.get_status()) 
    if(http.get_status() == HTTPClient.STATUS_CONNECTED): 
        online = true
        print("Online mode")# Make sure all is OK
        update_data()
        #var headers = [
            #"User-Agent: Pirulo/1.0 (Godot)",
            #"Accept: */*",
        #    "Content-Type: application/x-www-form-urlencoded",
        #]
#        var fields = {"username" : "user", "password" : "pass"}
#var queryString = httpClient.query_string_from_dict(fields)
#var headers = [ "Content-Length: " + str(queryString.length())]
#var result = httpClient.request(httpClient.METHOD_POST, "index.php", headers, queryString)
        #err = http.request(HTTPClient.METHOD_GET, "/ping", headers) # Request a page from the site (this one was chunked..)
       # if (err == OK): 
    
    pass # replace with function body
    
func update_data():
    
    var sslvalidate = false
    var urlbase = "http://"+prequel_server_url+":"+str(prequel_server_port)
    
    var username = 'tester' #debug
    var token = '123token321'
    var headers = [
        "Content-Type: application/x-www-form-urlencoded",
        "username: "+username,
        "token: "+token,
    ]
    
    var url = urlbase+"/ping"
    var reqbody = ""
    #print("url:[",url,"] headers:[",str(headers),"]sslvalidate:[",sslvalidate,"] method:[",str(HTTPClient.METHOD_GET),"] reqbody:[",reqbody,"]")
    $HTTPRequest.request(url,headers,sslvalidate,HTTPClient.METHOD_GET,reqbody)
    
    #$HTTPRequest.request("http://localhost:8000/ping")
    pass